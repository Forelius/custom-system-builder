/*
 * Copyright 2024 Jean-Baptiste Louvet-Daniel
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import { Modifier, Primitive, System } from '../definitions.js';
import { ComputablePhraseOptions } from '../formulas/ComputablePhrase.js';

/**
 * A function to compute a phrase
 */
export type ComputeFunction = (additionalProps?: System['props']) => Primitive;

/**
 * ComputableElement Interface
 * Implement this to have your component compute values on sheet loading
 */
export default interface ComputableElement {
    /**
     * Returns a record of functions indexed by component keys to compute the value of the component
     * @param entity The entity triggering the computation
     * @param modifiers The modifiers to apply to the computed value, if applicable
     * @param options Some additional options for computation
     * @param keyOverride Override the component's key if necessary. This new key must be used in place of the component's key if set.
     * @returns A record of functions indexed by component keys
     */
    getComputeFunctions(
        entity: TemplateSystem,
        modifiers: Record<string, Modifier[]>,
        options?: ComputablePhraseOptions,
        keyOverride?: string
    ): Record<string, ComputeFunction>;
}

/**
 * ComputableElementbar tester
 * @param element The element to test
 * @returns If the element implements ComputableElement
 */
export function isComputableElement(element: unknown): element is ComputableElement {
    return (element as ComputableElement).getComputeFunctions !== undefined;
}
