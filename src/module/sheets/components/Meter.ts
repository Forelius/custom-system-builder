/*
 * Copyright 2024 Jean-Baptiste Louvet-Daniel
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

/**
 * @ignore
 * @module
 */

import { Modifier, Primitive, System } from '../../definitions.js';
import { ComputablePhraseOptions } from '../../formulas/ComputablePhrase.js';
import AttributeBarElement from '../../interfaces/AttributeBarElement.js';
import ComputableElement, { ComputeFunction } from '../../interfaces/ComputableElement.js';
import Logger from '../../Logger.js';
import { applyModifiers } from '../../utils.js';
import { ComponentValueType } from './Component.js';
import Container from './Container.js';
import InputComponent, { InputComponentProps, InputComponentJson, ComponentSize } from './InputComponent.js';

export type MeterProps = InputComponentProps & {
    value: string;
    min?: string;
    max?: string;
    low?: string;
    high?: string;
    optimum?: string;
};

export type MeterJson = InputComponentJson & {
    value: string;
    min?: string;
    max?: string;
    low?: string;
    high?: string;
    optimum?: string;
};

class Meter extends InputComponent implements ComputableElement, AttributeBarElement {
    static valueType: ComponentValueType = 'none';

    /**
     * Meter value formula
     */
    protected _value: string;

    /**
     * Minimum value formula
     */
    protected _min?: string;

    /**
     * Maximum value formula
     */
    protected _max?: string;

    /**
     * Low value formula
     */
    protected _low?: string;

    /**
     * High value formula
     */
    protected _high?: string;

    /**
     * Optimum value formula
     */
    protected _optimum?: string;

    /**
     * Meter constructor
     */
    constructor(props: MeterProps) {
        super(props);

        this._value = props.value;
        this._min = props.min;
        this._max = props.max;
        this._low = props.low;
        this._high = props.high;
        this._optimum = props.optimum;
    }

    /**
     * Renders component
     * @override
     * @param entity Rendered entity (actor or item)
     * @param isEditable Is the component editable by the current user ?
     * @param options Additional options usable by the final Component
     * @return The jQuery element holding the component
     */
    protected async _getElement(
        entity: TemplateSystem,
        isEditable: boolean = true,
        options: ComputablePhraseOptions = { computeExplanation: false, availableKeys: [] }
    ): Promise<JQuery> {
        const { customProps = {}, linkedEntity, reference } = options;
        const formulaProps = foundry.utils.mergeObject(entity.system?.props ?? {}, customProps, { inplace: false });

        const jQElement = await super._getElement(entity, isEditable, options);

        const meterElement = $('<meter></meter>');

        if (entity.isTemplate) {
            meterElement.val(0.5);
            meterElement.append('0.5');

            jQElement.addClass('custom-system-editable-component');
            jQElement.on('click', () => {
                this.editComponent(entity);
            });
        } else {
            if (this._min) {
                const min = this._computeParam(this._min, entity, { ...options, source: `${this.key}.min` });
                meterElement.attr('min', parseFloat(min));
            }

            if (this._max) {
                const max = this._computeParam(this._max, entity, { ...options, source: `${this.key}.max` });
                meterElement.attr('max', parseFloat(max));
            }

            if (this._low) {
                const low = this._computeParam(this._low, entity, { ...options, source: `${this.key}.low` });
                meterElement.attr('low', parseFloat(low));
            }

            if (this._high) {
                const high = this._computeParam(this._high, entity, { ...options, source: `${this.key}.high` });
                meterElement.attr('high', parseFloat(high));
            }

            if (this._optimum) {
                const optimum = this._computeParam(this._optimum, entity, {
                    ...options,
                    source: `${this.key}.optimum`
                });
                meterElement.attr('optimum', parseFloat(optimum));
            }

            let meterValue = 0;
            // If Meter has a key, it was computed with the derivedData of the entity, no need to recompute it
            if (
                this.key &&
                foundry.utils.getProperty(formulaProps, this.key) !== null &&
                foundry.utils.getProperty(formulaProps, this.key) !== undefined
            ) {
                meterValue = foundry.utils.getProperty(formulaProps, this.key);
                Logger.debug('Using precomputed value for ' + this.key + ' : ' + meterValue);
            } else {
                try {
                    meterValue = parseFloat(
                        (
                            await ComputablePhrase.computeMessage(this._value ?? '', formulaProps, {
                                source: `${this.key}`,
                                reference,
                                defaultValue: '',
                                triggerEntity: entity,
                                linkedEntity
                            })
                        ).result
                    );
                } catch (err) {
                    Logger.error((err as Error).message, err);
                    meterValue = 0;
                }
            }

            meterElement.val(meterValue);
            meterElement.append(meterValue.toString());
        }

        jQElement.append(meterElement);

        return jQElement;
    }

    protected _computeParam(valueFormula: string, entity: TemplateSystem, options?: ComputablePhraseOptions) {
        const formulaProps = foundry.utils.mergeObject(entity.system?.props ?? {}, options?.customProps ?? {}, {
            inplace: false
        });

        return ComputablePhrase.computeMessageStatic(valueFormula, formulaProps, {
            ...options,
            defaultValue: 0,
            triggerEntity: entity
        }).result;
    }

    getComputeFunctions(
        entity: TemplateSystem,
        modifiers: Record<string, Modifier[]>,
        options?: ComputablePhraseOptions,
        keyOverride?: string
    ): Record<string, ComputeFunction> {
        const computationKey = keyOverride ?? this.key;

        if (!computationKey) {
            return {};
        }

        return {
            [computationKey]: (additionalProps?: System['props']) => {
                const formulaProps = foundry.utils.mergeObject(
                    foundry.utils.mergeObject(entity.system.props ?? {}, options?.customProps, {
                        inplace: false
                    }),
                    additionalProps,
                    {
                        inplace: false
                    }
                );

                let value: number = parseFloat(
                    ComputablePhrase.computeMessageStatic(this._value ?? '', formulaProps, {
                        ...options,
                        source: computationKey,
                        availableKeys: Object.keys(formulaProps),
                        triggerEntity: entity
                    }).result
                );

                if (modifiers[computationKey]) {
                    value = parseFloat(applyModifiers(value as Primitive, modifiers[computationKey])?.toString() ?? '');
                }

                return value;
            }
        };
    }

    getMaxValue(entity: TemplateSystem, options?: ComputablePhraseOptions, keyOverride?: string): number {
        return parseFloat(
            this._computeParam(this._max ?? '0', entity, { ...options, source: `${keyOverride ?? this.key}.max` })
        );
    }

    getValue(entity: TemplateSystem, _options?: ComputablePhraseOptions, keyOverride?: string): number {
        return entity.system.props[keyOverride ?? this.key!] as number;
    }

    /**
     * Returns serialized component
     * @override
     */
    toJSON(): MeterJson {
        const jsonObj = super.toJSON();

        return {
            ...jsonObj,
            value: this._value,
            min: this._min,
            max: this._max,
            low: this._low,
            high: this._high,
            optimum: this._optimum,
            type: 'meter'
        };
    }

    /**
     * Creates meter from JSON description
     * @override
     */
    static fromJSON(json: MeterJson, templateAddress: string, parent: Container): Meter {
        return new Meter({
            key: json.key,
            tooltip: json.tooltip,
            templateAddress: templateAddress,
            label: json.label,
            value: json.value,
            min: json.min,
            max: json.max,
            low: json.low,
            high: json.high,
            optimum: json.optimum,
            size: json.size,
            cssClass: json.cssClass,
            role: json.role,
            permission: json.permission,
            visibilityFormula: json.visibilityFormula,
            parent: parent
        });
    }

    /**
     * Gets pretty name for this component's type
     * @return The pretty name
     * @throws {Error} If not implemented
     */
    static getPrettyName(): string {
        return 'Meter';
    }

    /**
     * Get configuration form for component creation / edition
     * @return The jQuery element holding the component
     */
    static async getConfigForm(existingComponent: MeterJson, _entity: TemplateSystem): Promise<JQuery> {
        const mainElt = $('<div></div>');

        const predefinedValuesComponent = { ...existingComponent };

        mainElt.append(
            await renderTemplate(
                `systems/${game.system.id}/templates/_template/components/meter.html`,
                predefinedValuesComponent
            )
        );

        return mainElt;
    }

    /**
     * Extracts configuration from submitted HTML form
     * @override
     * @param html The submitted form
     * @return The JSON representation of the component
     * @throws {Error} If configuration is not correct
     */
    static extractConfig(html: JQuery): MeterJson {
        const superFieldData = super.extractConfig(html);
        const fieldData: MeterJson = {
            ...superFieldData,
            type: 'meter',
            label: html.find('#meterLabel').val()?.toString() ?? '',
            size: (html.find('#meterSize').val()?.toString() as ComponentSize) ?? 'full-size',
            value: html.find('#meterValue').val()?.toString() ?? '',
            min: html.find('#meterMin').val()?.toString() ?? '',
            max: html.find('#meterMax').val()?.toString() ?? '',
            low: html.find('#meterLow').val()?.toString() ?? '',
            high: html.find('#meterHigh').val()?.toString() ?? '',
            optimum: html.find('#meterOptimum').val()?.toString() ?? ''
        };

        return fieldData;
    }
}

/**
 * @ignore
 */
export default Meter;
